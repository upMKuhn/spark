
FROM bitnami/spark:3.2.0 as Bitnami

FROM gcr.io/spark-operator/spark-py:v3.1.1-hadoop3
USER root
RUN rm -rf /opt/spark
COPY --from=Bitnami /opt/bitnami/spark/ /opt/spark/

#RUN pip install wget
#RUN python3 -m wget https://archive.apache.org/dist/spark/spark-3.2.0/spark-3.2.0-bin-hadoop3.2.tgz
#RUN python3 -m wget https://repo1.maven.org/maven2/org/apache/hadoop/hadoop-aws/3.2.0/hadoop-aws-3.2.0.jar
#RUN python3 -m wget https://repo1.maven.org/maven2/com/amazonaws/aws-java-sdk/1.11.375/aws-java-sdk-1.11.375.jar

#RUN rm $SPARK_HOME/jars/*
#RUN cp ./hadoop-aws-3.2.0.jar $SPARK_HOME/jars
#RUN cp ./aws-java-sdk-1.11.375.jar $SPARK_HOME/jars
#RUN tar -xvzf ./spark-3.2.0-bin-hadoop3.2.tgz -C ./
#RUN cp ./spark-3.2.0-bin-hadoop3.2/jars/* /opt/spark/jars
#RUN PYSPARK_HADOOP_VERSION=3.2 pip install pyspark[sql]==3.2.0
#USER 185
